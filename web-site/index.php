<?php

require "controller/fronted.php";

if (isset($_GET["content"]))
{
  if ($_GET["content"] == "connexion")
  {
    login();
  }
  elseif ($_GET["content"] == "calendrier")
  {
    calendar();
  }
  elseif ($_GET["content"] == "creation")
  {
    creation();
  }
  elseif ($_GET["content"] == "publication")
  {
    publication();
  }
  elseif ($_GET["content"] == "compte")
  {
    account();
  }
  elseif ($_GET["content"] == "ecran")
  {
    display();
  }
  elseif ($_GET["content"] == "parametres")
  {
    setting();
  }
  elseif ($_GET["content"] == "mon-profil")
  {
    myAccount();
  }
  else
  {
    echo "Erreur : Une erreur est survenue\n la page rechercher n'existe pas.";
  }
}
else
{
  home();
}

?>
