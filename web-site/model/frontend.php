<?php

function getPublications()
{
  $db = dbConnect();

  $req = $db->query("SELECT * FROM publication");

  return $req;
}

function searchUnknow()
{
 $result = exec("sudo /home/pi/Documents/software/search.sh");
 $raspberry = explode("Raspberry Pi Foundation", $result);


 $db = dbConnect();

 $counter = 0;
 for ($i=0; isset($raspberry[$i]) ; $i++)
 {
  // save the mac adress
  $raspberry[$i] = substr($raspberry[$i], -18, -1);


  $req = $db->prepare("SELECT id FROM display WHERE mac = ?");
  $req->execute(array($raspberry[$i]));
  $out = $req->fetchAll();

  $req->closeCursor();

  if ($out == false)
  {
    if (!isset($raspberryUnknow[0]))
    {
      $counter=0;
    }

    $raspberryUnknow[$counter] = $raspberry[$i];
    $counter++;
  }



}

return $raspberryUnknow;
}

function searchKnow()
{
 $db = dbConnect();

 $raspberryKnow = $db->query('SELECT * FROM display ORDER BY id');

 return $raspberryKnow;
}

function dbConnect()
{
  try
  {
    $db = new PDO("mysql:host=localhost;dbname=dynamic-display;charset=utf8", "root", "Pe33ju82&*");

    return $db;
  }
  catch(Exception $e)
  {
    die("Erreur : ".$e->getMessage());
  }

}
