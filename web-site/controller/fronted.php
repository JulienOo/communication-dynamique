<?php

require "model/frontend.php";

function login()
{
  require "view/frontend/login.php";
}

function home()
{
  require "view/frontend/home.php";
}

function creation()
{
  require "view/frontend/creation.php";
}

function publication()
{
  $publications = getPublications();

  require "view/frontend/publication.php";
}

function account()
{
  require "view/frontend/account.php";
}

function display()
{
 $displayUnknow = searchUnknow();

 $displayKnow = searchKnow();


 require "view/frontend/display.php";
}

function setting()
{
  require "view/frontend/setting.php";
}

function myAccount()
{
  require "view/frontend/myAccount.php";
}

?>
